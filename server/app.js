const express =  require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors')

mongoose.Promise = global.Promise;

if (process.env.NODE_ENV === 'test') {
    // mongoose.connect('mongodb://localhost/APIAuthTEST', { useCreateIndex: true,  useNewUrlParser: true });
    mongoose.connect('mongodb+srv://admin:admin@mongodb-umfhe.azure.mongodb.net/APIAuthTEST?retryWrites=true', { useCreateIndex: true,  useNewUrlParser: true });
  } else {
    // mongoose.connect('mongodb://localhost/APIAuth', { useCreateIndex: true,  useNewUrlParser: true });
    mongoose.connect('mongodb+srv://admin:admin@mongodb-umfhe.azure.mongodb.net/APIAuth?retryWrites=true', { useCreateIndex: true,  useNewUrlParser: true });
  }





// if (process.env.NODE_ENV === 'prod') {

//  // mongoose.connect('mongodb+srv://admin:admin@mongodb-umfhe.azure.mongodb.net/APIAuth?retryWrites=true', { useCreateIndex: true,  useNewUrlParser: true });

// } else if (process.env.NODE_ENV === 'test_prod') {

//  // mongoose.connect('mongodb+srv://admin:admin@mongodb-umfhe.azure.mongodb.net/APIAuthTEST?retryWrites=true', { useCreateIndex: true,  useNewUrlParser: true });

// } else if (process.env.NODE_ENV === 'test') {

//   mongoose.connect('mongodb://localhost/APIAuthTEST', { useCreateIndex: true,  useNewUrlParser: true });
//   console.log("Connected MongoDB local for testing");

// } else {

//   mongoose.connect('mongodb://localhost/APIAuth', { useCreateIndex: true,  useNewUrlParser: true });
//   console.log("Connected MongoDB local for dev")

// }








// mongoose.connect('mongodb://localhost/APIAuth', { useCreateIndex: true, useNewUrlParser: true });

const app = express();
app.use(cors());

// Middleware
if (!process.env.NODE_ENV === 'test') {
  app.use(morgan('dev'));
}

if (process.env.NODE_ENV === 'production') {
  // Exprees will serve up production assets
  app.use(express.static('client/build'));

  // Express serve up index.html file if it doesn't recognize route
  const path = require('path');
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}



app.use(bodyParser.json());

  // Routers
  app.use('/users', require('./routes/users'));



  

// if (process.env.NODE_ENV === 'production') {
//   // Exprees will serve up production assets
//   app.use(express.static('../client/build'));
//   // console.log(express.static('../client/build'));

//   // Express serve up index.html file if it doesn't recognize route
//   const path = require('path');
//   app.get('*', (req, res) => {
//     res.sendFile(path.resolve(__dirname, '../client/build/index.html'));
//     console.log(path.resolve(__dirname, '../client/build/index.html'))
//   });




// }






// Start the server
// const port = process.env.PORT || 3000;
// app.listen(port);
// console.log(`Server listening ad ${port}`);





module.exports = app;