# Node & React Login API

Es un ejemplo de login utilizando paquetes diseñados para la autenticación, así como controles para el registro de una nueva cuenta (email únicos), las contraseñas están encriptadas y almacenadas en una Base de Datos. Para dicho proyecto es utilizado la herramiento de CI/CD proveida por GitLab para su deploy a Heroku luego de que pasen los test.

## Getting Started



Para probar el proyecto se necesitan estos paquetes

```
npm = ^6.9.0
node = ^10.15.3
```

### Installing

Mediante estos comandos se podrán instalar las dependencias del proyecto

```bash
# Install dependencies
npm install
npm install --prefix client

# the frond-end serve on localhost:3000 
# the back-end serve on localhost:5000
npm run start
```


## Running the tests

El proyecto tiene una serie de test que prueban tanto el login exitoso como uno incorrecto, así como también la disponibilidad de los endpoints


```
npm run test
```

## Deployment

El deploy se realiza mediando GitLab CI/CD a Heroku. En el archivo .gitlab-ci.yml se encuentran los parametros utilizados para el CI/CD.

* Por cada Commit a master, el CI/CD actua y lo envia a staging
* Por cada Tag de master, el CI/CD actua y lo envia a production

## Built With

* [React](https://reactjs.org/) - The library for building user interfaces
* [Nodejs](https://nodejs.org/en/) -  JavaScript runtime,
* [Mongo](https://www.mongodb.com/) - NoSQL DataBase


## Versionamiento

Se utilizo [GitLab](https://gitlab.com/) para el versionamiento.

## Authors

* **Juan Acuña** - *Initial work* - [jacunapy](https://gitlab.com/jacunapy)

## Observaciones
Como se utiliza Mongodb Atlas, al correr los test, el usuario creado no es borrado como esta dispuesto en el test, ya que los database alojados en Atlas no aceptan el comando de mongoose para borrar la base de datos. Dicho problema no sucede cuando es ejecutado localmente. Por lo tanto para correr los test de manera repetitiva así como también pasar el proyecto a producción hay que borrar manualmente el usuario utilizado para la prueba.



