import axios from 'axios';
// import port from '../../../server/index';
import { AUTH_SIGN_UP, AUTH_ERROR, AUTH_SIGN_OUT, AUTH_SIGN_IN, DASHBOARD_GET_DATA } from './types';

// const port = process.env.PORT || 5000;
export const signUp = data  => {
    return  async dispatch => {

       try {
           console.log('[ActionCreator] signUp called');

           if (process.env.NODE_ENV === 'production') {
               const res = await axios.post('https://authpi.herokuapp.com/users/signup', data);
               dispatch({
                type: AUTH_SIGN_UP,
               payload: res.data
           });
                localStorage.setItem('JWT_TOKEN', res.data.token);
                axios.defaults.headers.common['Authorization'] = '';
           } else {
               const res  = await axios.post('http://localhost:5000/users/signup', data);
               dispatch({
                type: AUTH_SIGN_UP,
               payload: res.data
           });
                localStorage.setItem('JWT_TOKEN', res.data.token);
                axios.defaults.headers.common['Authorization'] = '';
           }
        //    const res = await axios.post('https://authpi.herokuapp.com/users/signup', data);
        //    const res = await axios.post('http://localhost:5000/users/signup', data);

           console.log('[ActionCreator] signUp dispatched an action ');

        //   dispatch({
        //      type: AUTH_SIGN_UP,
        //     payload: res.data
        // });


        // localStorage.setItem('JWT_TOKEN', res.data.token);
        // axios.defaults.headers.common['Authorization'] = '';

       } catch (err) {
        dispatch({
            type: AUTH_ERROR,
            payload: 'Email is already in use'
        })
       }
    };
}

export const getSecret = () => {
    return async dispatch => {

        try {
            console.log('[ActionCreator] Trying to get BE\'s secret');

            if (process.env.NODE_ENV === 'production') {
                const res = await axios.get('https://authpi.herokuapp.com/users/secret');
                console.log('res', res);

                dispatch({
                    type: DASHBOARD_GET_DATA,
                    payload: res.data.secret
                })
            } else {

                const res = await axios.get('http://localhost:5000/users/secret');
                console.log('res', res);

                dispatch({
                    type: DASHBOARD_GET_DATA,
                    payload: res.data.secret
            })


            }
            // const res = await axios.get('https://authpi.herokuapp.com/users/secret');
            // const res = await axios.get('http://localhost:5000/users/secret');
            // console.log('res', res);

            // dispatch({
            //     type: DASHBOARD_GET_DATA,
            //     payload: res.data.secret
            // })

        } catch (err) {
            console.error('err', err);
        }

    }
}

export const signOut = () => {
    return dispatch => {
        localStorage.removeItem('JWT_TOKEN');
        axios.defaults.headers.common['Authorization'] = '';

        dispatch ({
            type: AUTH_SIGN_OUT,
            payload: ''
        })
    };
}

export const signIn = data  => {
    return  async dispatch => {

       try {
           console.log('[ActionCreator] signIn called');

           if (process.env.NODE_ENV === 'production') {
            const res = await axios.post('https://authpi.herokuapp.com/users/signin', data);

            console.log('[ActionCreator] signIn dispatched an action ');

            dispatch({
                type: AUTH_SIGN_IN,
                payload: res.data
            });

            localStorage.setItem('JWT_TOKEN', res.data.token);
            axios.defaults.headers.common['Authorization'] = '';
           } else {
                const res = await axios.post('http://localhost:5000/users/signin', data);

                console.log('[ActionCreator] signIn dispatched an action ');

                dispatch({
                        type: AUTH_SIGN_IN,
                    payload: res.data
                });
   
        
                localStorage.setItem('JWT_TOKEN', res.data.token);
                axios.defaults.headers.common['Authorization'] = '';

           }
        //    const res = await axios.post('https://authpi.herokuapp.com/users/signin', data);
        //    const res = await axios.post('http://localhost:5000/users/signin', data);

        //    console.log('[ActionCreator] signIn dispatched an action ');

        //   dispatch({
        //      type: AUTH_SIGN_IN,
        //     payload: res.data
        // });


        // localStorage.setItem('JWT_TOKEN', res.data.token);
        // axios.defaults.headers.common['Authorization'] = '';
       } catch (err) {
        dispatch({
            type: AUTH_ERROR,
            payload: 'Email & password combination isn\'t valid'
        })
       }
    };
}